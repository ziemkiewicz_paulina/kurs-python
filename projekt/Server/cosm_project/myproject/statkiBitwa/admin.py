from django.contrib import admin
from .models import NewGame
# Register your models here.

class NewGameAdmin(admin.ModelAdmin):
    pass

admin.site.register(NewGame, NewGameAdmin)
