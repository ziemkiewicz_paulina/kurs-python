from django.db import models


class NewGame(models.Model):
    id = models.AutoField(primary_key=True)
    dataDodania = models.DateTimeField(auto_now_add=True, blank=False)
    gracz1 = models.CharField(max_length=40, blank=False, null=False)
    gracz2 = models.CharField(max_length=40, blank=False, null=False)
    nazwaGry = models.CharField(max_length=40, blank=False, null=False)
    gracz1Plansza = models.CharField(max_length=2000)
    gracz2Plansza = models.CharField(max_length=2000)
    gracz2Dolaczyl = models.BooleanField(blank=False)
    gracz2DostarczylMape = models.BooleanField(blank=False)
    gracz1Tura = models.BooleanField(blank=False, default=True)
    gracz1Przegral = models.BooleanField(blank=False, default=False)
    gracz2Przegral = models.BooleanField(blank=False, default=False)

    def __str__(self):
        return 'gre zalozyl: ' + str(self.gracz1) + ' nazwa gry: ' + str(self.nazwaGry)
