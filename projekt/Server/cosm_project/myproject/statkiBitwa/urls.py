from django.urls import path, include
from . import views

urlpatterns = [
    path('getListOfGames', views.listGames),
    path('startNewGame', views.createGame),
    path('joinGame', views.joinGame),
    path('checkForSecondPlayer', views.waitingForPlayer),
    path('getPlayerBoard', views.setPlayerBoard),
    path('getEnemyBoard', views.getEnemyBoard),
    path('checkTurn', views.checkTurn),
    path('endTurn', views.endTurn),
    path('sprawdzenieWynikuGry', views.amIVictorius),
    path('przeslanieWynikuGry', views.IamVictorius)

]