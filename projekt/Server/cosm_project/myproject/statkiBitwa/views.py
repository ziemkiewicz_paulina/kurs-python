from .models import NewGame
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
import json


@csrf_exempt
def listGames(request):
    newGames = NewGame.objects.filter(gracz2Dolaczyl=False)
    listaGier = {"dostepneGry": []}
    for game in newGames:
        singleGame = {
            'game_name': game.nazwaGry,
            'enemy': game.gracz1
        }
        listaGier["dostepneGry"].append(singleGame)
    return JsonResponse(listaGier)


@csrf_exempt
def createGame(request):
    try:
        postMessage = json.loads(request.body.decode('utf8'))
        newGame = NewGame()
        newGame.gracz1 = postMessage["playerName"]
        newGame.nazwaGry = postMessage["gameName"]
        newGame.gracz2DostarczylMape = False
        newGame.gracz2Dolaczyl = False
        newGame.save()
        return JsonResponse({"Status": "new game recived"})
    except Exception as e:
        return JsonResponse({"error": e})


@csrf_exempt
def joinGame(request):
    try:
        postMessage = json.loads(request.body.decode('utf8'))
        try:
            newGames = NewGame.objects.get(nazwaGry=postMessage["gameName"])
            newGames.gracz2 = postMessage['playerName']
            newGames.gracz2Dolaczyl = True
            newGames.save()
        except:
            return JsonResponse({"Status": "no game fit to recived json"})
        return JsonResponse({"Status": "you joined the selected game"})
    except:
        return JsonResponse({"error": "no POST message delivered"})

@csrf_exempt
def recivePlayersBoard(request):
    try:
        postMessage = request.POST
        try:
            newGames = NewGame.objects.get(nazwaGry=postMessage["gameName"])
            if newGames.gracz2 == request.POST['playerName']:
                newGames.gracz2Plansza = request.POST['board']
            if newGames.gracz1 == request.POST['playerName']:
                newGames.gracz1Plansza = request.POST['board']
            newGames.save()
        except:
            return JsonResponse({"Status": "no game fit to recived json"})
        return JsonResponse({"Status": "boardUpdateRecived"})
    except:
        return JsonResponse({"error": "no POST message delivered"})

@csrf_exempt
def getEnemyBoard(request):
    try:
        postMessage = json.loads(request.body.decode('utf8'))
        try:
            newGames = NewGame.objects.get(nazwaGry=postMessage["gameName"])
        except:
            return JsonResponse({"Status": "no game fit to recived json: "+ postMessage["gameName"]})

        if newGames.gracz1 == postMessage['playerName']:
            return JsonResponse(
                {
                    'board': newGames.gracz2Plansza
                }
            )
        if newGames.gracz2 == postMessage['playerName']:
            return JsonResponse(
                {
                    'board': newGames.gracz1Plansza
                }
            )
    except:
        return JsonResponse({"error": "no POST message delivered: "+postMessage})


@csrf_exempt
def waitingForPlayer(request):
    postMessage = json.loads(request.body.decode('utf8'))
    nazwaGry = postMessage["gameName"]
    try:
        newGame = NewGame.objects.get(nazwaGry=nazwaGry)
        if not newGame:
            return JsonResponse({"error": "Nie znaleziono gry o podanym ID"})
        if newGame.gracz2Dolaczyl == True:
            if newGame.gracz2DostarczylMape == False:
                return JsonResponse({'message': 'Oczekiwanie na przygotowanie planszy przez przeciwnika'})
            return JsonResponse({'message': 'ready'})
        return JsonResponse({'message': 'Oczekiwanie na 2 gracza'})
    except Exception as e:
        print(e)
        return JsonResponse({"server error": e})

@csrf_exempt

def setPlayerBoard(request):
    try:
        postMessage = json.loads(request.body.decode('utf8'))
        nazwaGry = postMessage["gameName"]
        newGame = NewGame.objects.get(nazwaGry=nazwaGry)
        if newGame.gracz1 == postMessage["playerName"]:
            newGame.gracz1Plansza = postMessage["board"]
        else:
            newGame.gracz2Plansza = postMessage["board"]
            newGame.gracz2DostarczylMape = True
        newGame.save()
        return JsonResponse({"Status": "board updated"})
    except Exception as e:
        return JsonResponse({"error": e})

@csrf_exempt
def checkTurn(request):
    try:
        postMessage = json.loads(request.body.decode('utf8'))
        try:
            newGames = NewGame.objects.get(nazwaGry=postMessage["gameName"])
            if newGames.gracz2 == postMessage['playerName']:
                return JsonResponse({"mojaTura": not newGames.gracz1Tura})
            if newGames.gracz1 == postMessage['playerName']:
                return JsonResponse({"mojaTura": newGames.gracz1Tura})
        except:
            return JsonResponse({"Status": "no game fit to recived json"})
    except:
        return JsonResponse({"error": "no POST message delivered"})

@csrf_exempt
def endTurn(request):
    try:
        postMessage = json.loads(request.body.decode('utf8'))
        try:
            newGames = NewGame.objects.get(nazwaGry=postMessage["gameName"])
            if newGames.gracz2 == postMessage['playerName']:
                newGames.gracz1Tura = True
            if newGames.gracz1 == postMessage['playerName']:
                newGames.gracz1Tura = False
            newGames.save()
        except:
            return JsonResponse({"Status": "no game fit to recived json"})
        return JsonResponse({"Status": "you joined the selected game"})
    except:
        return JsonResponse({"error": "no POST message delivered"})

@csrf_exempt
def amIVictorius(request):
    try:
        postMessage = json.loads(request.body.decode('utf8'))
        try:
            newGames = NewGame.objects.get(nazwaGry=postMessage["gameName"])
            if newGames.gracz2 == postMessage['playerName'] and newGames.gracz2Przegral:
                return JsonResponse({"wynikGry": "Przegrałeś!"})
            elif newGames.gracz2 == postMessage['playerName'] and newGames.gracz1Przegral:
                return JsonResponse({"wynikGry": "Wygraleś!"})
            elif newGames.gracz1 == postMessage['playerName'] and newGames.gracz1Przegral:
                return JsonResponse({"wynikGry": "Przegrałeś!"})
            elif newGames.gracz1 == postMessage['playerName'] and newGames.gracz2Przegral:
                return JsonResponse({"wynikGry": "Wygraleś!"})
            else:
                return JsonResponse({'wynikGry':'graNierozstrzygnieta'})
        except:
            return JsonResponse({"Status": "no game fit to recived json"})
        return JsonResponse({"Status": "you joined the selected game"})
    except:
        return JsonResponse({"error": "no POST message delivered"})

@csrf_exempt
def IamVictorius(request):
    try:
        postMessage = json.loads(request.body.decode('utf8'))
        try:
            newGames = NewGame.objects.get(nazwaGry=postMessage["gameName"])
            if newGames.gracz2 == postMessage['playerName']:
                newGames.gracz1Przegral = True
            if newGames.gracz1 == postMessage['playerName']:
                newGames.gracz2Przegral = True
            newGames.save()
        except:
            return JsonResponse({"Status": "no game fit to recived json"})
        return JsonResponse({"Status": "you joined the selected game"})
    except:
        return JsonResponse({"error": "no POST message delivered"})