import requests
import json
import random
import sys
import enum
import time
# To jest po prostu slownik do ktorego mozemy dynamicznie dokladac klucze. Bardzo wygodne kiedy nie wiesz jakie klucze
# chcesz wrzucic, tylko np wrzucasz w petli
from collections import defaultdict


def stringToEnum(stringInJson):
    translateDictionary = {
        'X': ShipStatus.Hit,
        'O': ShipStatus.Miss,
        '~': ShipStatus.Water,
        'Q': ShipStatus.Own,
        'U': ShipStatus.Enemy,
    }
    return translateDictionary[stringInJson]


def createBoardFromJson(jsonText):
    boardInArray = json.loads(json.loads(jsonText)['board'])
    newBoard = [[0 for _ in range(len(boardInArray))] for _ in range(len(boardInArray[0]))]
    iCounter = 0
    jCounter = 0
    for i in boardInArray:
        for j in i:
            newShipStatus = stringToEnum(j)
            newBoard[iCounter][jCounter] = newShipStatus
            jCounter += 1
        jCounter = 0
        iCounter += 1
    return newBoard


class ShipStatus(str, enum.Enum):
    Hit = 'X'
    Miss = 'O'
    Water = '~'
    Own = 'Q'
    Enemy = 'U'


class ShipTypes(enum.Enum):
    # Carrier = 5
    # Battleship = 4
    # Cruiser = 3
    # Submarine = 3
    Destroyer = 2
    # Test = 1


class Board(object):
    def __init__(self):
        board_height = 10
        board_length = 10
        self.board = [[ShipStatus.Water for _ in range(board_length)] for x in range(board_height)]
        # Ta zmienna jest tylko i wylacznie w celu latwego sprawdzenia czy zatopiliscmy okret czy tylko trafilismy
        self.ships_cords = defaultdict(list)

    def _is_game_finished(self):
        ships_sum = 0
        for array in self.board:
            ships_sum += array.count(ShipStatus.Own) + array.count(ShipStatus.Enemy)
        return ships_sum <= 0

    def _is_hit(self, x, y):
        return self.board[y][x] == ShipStatus.Enemy or self.board[y][x] == ShipStatus.Own

    def _sink_ship(self, x, y):
        self.board[y][x] = ShipStatus.Hit

    def print_board(self):
        print("\n{}".format("Plansza".upper()))
        i = 0
        print('       ', end='')
        print('   '.join(str(x) for x in range(len(self.board))))
        for row in self.board:
            array = [status.value for status in row]
            print('{}    | '.format(i), end='')
            print(' | '.join(array), end='')
            print(' |    {}'.format(i))
            i += 1
        print('       ', end='')
        print('   '.join(str(x) for x in range(len(self.board))))

    def print_board_hide_ships(self):
        print("\n{}".format("Plansza".upper()))
        i = 0
        print('       ', end='')
        print('   '.join(str(x) for x in range(len(self.board))))
        for row in self.board:
            array = [status.value for status in row]
            print('{}    | '.format(i), end='')
            print(' | '.join(array).replace("Q", "~"), end='')
            print(' |    {}'.format(i))
            i += 1
        print('       ', end='')
        print('   '.join(str(x) for x in range(len(self.board))))

    def _is_not_ship_near_another(self, actual_ship_cord, y, x):
        # patrzymy co jest juz na planszy na wszystkich sasiednich wspolrzednych, ale jesli to wspolrzedna aktualnie stawianego
        board_values = [
            # Po skosie nie mozna!
            self.board[y - 1][x - 1] if [y - 1, x - 1] not in actual_ship_cord else ShipStatus.Water,
            self.board[y - 1][x + 1] if [y - 1, x + 1] not in actual_ship_cord else ShipStatus.Water,
            self.board[y + 1][x - 1] if [y + 1, x - 1] not in actual_ship_cord else ShipStatus.Water,
            self.board[y + 1][x + 1] if [y + 1, x + 1] not in actual_ship_cord else ShipStatus.Water,
            # Przylegajace caly bokiem tez nie
            self.board[y - 1][x] if [y - 1, x] not in actual_ship_cord else ShipStatus.Water,
            self.board[y + 1][x] if [y + 1, x] not in actual_ship_cord else ShipStatus.Water,
            self.board[y][x - 1] if [y, x - 1] not in actual_ship_cord else ShipStatus.Water,
            self.board[y][x + 1] if [y, x + 1] not in actual_ship_cord else ShipStatus.Water,
        ]
        return ShipStatus.Own not in board_values


class PlayerBoard(Board):
    def __init__(self):
        Board.__init__(self)
        self.number_of_ships = sum([x.value for x in ShipTypes])
        self.board_name = "Own Board"
        self._create_own_board()

    def _create_own_board(self):
        ships_deployed = []
        self.print_board()
        while not self._are_all_ships_deployed(ships_deployed):
            type_of_ship = input(
                "Podaj typ okrętu, ktory chcesz postawic. Mozliwe typy: {}: ".format(
                    ', '.join(
                        "{} ({})".format(x.name, x.value) for x in ShipTypes if x.name not in ships_deployed
                    )
                )
            )
            if type_of_ship not in [x.name for x in ShipTypes]:
                print("Podales zly typ okretu")
                continue
            # liczba czesci statku, dla podstawowego statku to 1
            i = 1
            # przechowujemy wspolrzedne aktualnego statku, ktory stawiamy na planszy (czyli wspolrzedne wszystkich elementow
            # zeby sprawdzic czy przylegaja do siebie
            actual_ship_cord = []
            while True:
                # rozmiar statku wzietu z enuma
                ship_size = ShipTypes[type_of_ship].value
                coordinates = input("Podaj wspolrzedne X,Y na jakich chcesz postawic element okretu, np. 3,4: ")
                try:
                    x, y = coordinates.split(',')
                    x = int(x)
                    y = int(y)
                except ValueError:
                    print("Prosze, podaj poprawne wspolrzedne.")
                    continue
                try:
                    # sprawdzamy czy pole w ktore chcemy polozyc statek jest puste (czyli wartosc 0)
                    if self.board[y][x] == ShipStatus.Water:
                        if not self._is_not_ship_near_another(actual_ship_cord, y, x):
                            print("Kolejne czesci okretu nie moga sie stykac z innymi okretami.")
                            continue
                        if not actual_ship_cord or self._is_not_ship_splitted(actual_ship_cord, y, x):
                            actual_ship_cord.append([y, x])
                            self.ships_cords[type_of_ship].append([y, x])
                            # ustawiamy na planszy element statku
                            self.board[y][x] = ShipStatus.Own
                        else:
                            print("Kolejne czesci okretu musza do siebie przylegac. Inaczej marynarze zgina :(")
                            continue
                    else:
                        print("To pole jest juz zajete. Prosze podaj inne wspolrzedne.")
                        continue
                except IndexError:
                    print("Podaj prosze wspolrzedna znajdujace sie na planszy.")
                    continue
                self.print_board()
                # jesli postawilismy tyle elementow co rozmiar okretu konczymy ten okret
                if i >= ship_size:
                    ships_deployed.append(type_of_ship)
                    break
                i += 1

    def _is_not_ship_splitted(self, actual_ship_cord, y, x):
        for cord in actual_ship_cord:
            x_cord = cord[1]
            y_cord = cord[0]
            y_ok = y in (y_cord - 1, y_cord + 1)
            x_ok = x in (x_cord - 1, x_cord + 1)
            # Jesli y jest ok, to nie mozemy pozwolic aby x bylo tez ok, bo wtedy umozliwiamy stawianie statkow po skosie
            if y_ok != x_ok:
                return True
        else:
            return False

    def _are_all_ships_deployed(self, ships_deployed):
        ships_deployed = set(ships_deployed)
        ships_to_deploy = set(x.name for x in ShipTypes)
        # robimy 2 sety i patrzymy na czesc wspolna. Jesli czesc wspolna jest taka sama jak caly set 2 to znaczy, ze caly set 2
        # znajduje sie w secie drugim (najszybsze sprawdzenie, bo wywalamy powtarzajace sie elementy, a przy tym sety sa szybkie
        common_ships = ships_deployed.intersection(ships_to_deploy)
        return common_ships == ships_to_deploy

    def _is_full(self):
        ships_sum = 0
        for array in self.board:
            ships_sum += array.count(ShipStatus.Own)
        return ships_sum >= self.number_of_ships

    def _players_turn(self, gameName, playerName, conector):
        print("TWÓJ RUCH")
        self.print_board_hide_ships()
        while True:
            coordinates = input("Podaj wspolrzedne X,Y na ktore chcesz strzelic: ")
            try:
                x, y = coordinates.split(',')
                x = int(x)
                y = int(y)
            except ValueError:
                print("Prosze, podaj poprawne wspolrzedne.")
                continue
            endTurnFlag = self._shot_field(x, y)
            self.print_board_hide_ships()
            if self._is_game_finished():
                conector.uploadGameResult(gameName, playerName)
                if conector.checkGameResult(gameName, playerName)['wynikGry'] != "graNierozstrzygnieta":
                    print(conector.checkGameResult(gameName, playerName)['wynikGry'])
                    print("Zapraszamy ponownie!")
                    sys.exit()
            if endTurnFlag:
                break

    def _shot_field(self, x, y):
        if self._was_shot_before(x, y):
            print("Juz strzelales w to pole.")
            return False
        elif self._is_hit(x, y):
            self._sink_ship(x, y)
            print("Trafiony")
            self._fill_rest_board_if_ship_sunk()
            return True
        else:
            self.board[y][x] = ShipStatus.Miss
            print("Nie trafiony")
            return True

    def _fill_rest_board_if_ship_sunk(self):
        # funkcja do sprawdzenia czy zatopilismy okret, jesli tak to ustawiamy wszystko na okolo na Miss
        for ship_cords in self.ships_cords.keys():
            for cords in self.ships_cords[ship_cords]:
                if self.board[cords[0]][cords[1]] != ShipStatus.Hit:
                    break
            # jesli jestesmy tutaj oznacza to, ze for poprzedni nie zostal "zbreakowany" czyli statek zostal zatopiony!
            else:
                # no to lecimy, przelatujemy po caly okrecie i oznaczamy wszystko na okolo na miss, chyba ze to sam okret
                for cord in self.ships_cords[ship_cords]:
                    x = cord[1]
                    y = cord[0]
                    # zabezpieczenie, zeby nie stawialo 0 po drugiej stronie planszy jesli dostanie male wartosci y
                    self.board[y - 1][x - 1] = ShipStatus.Miss if self.board[y - 1][
                                                                      x - 1] != ShipStatus.Hit else ShipStatus.Hit
                    self.board[y - 1][x + 1] = ShipStatus.Miss if self.board[y - 1][
                                                                      x + 1] != ShipStatus.Hit else ShipStatus.Hit
                    self.board[y + 1][x - 1] = ShipStatus.Miss if self.board[y + 1][
                                                                      x - 1] != ShipStatus.Hit else ShipStatus.Hit
                    self.board[y + 1][x + 1] = ShipStatus.Miss if self.board[y + 1][
                                                                      x + 1] != ShipStatus.Hit else ShipStatus.Hit
                    self.board[y - 1][x] = ShipStatus.Miss if self.board[y - 1][x] != ShipStatus.Hit else ShipStatus.Hit
                    self.board[y + 1][x] = ShipStatus.Miss if self.board[y + 1][x] != ShipStatus.Hit else ShipStatus.Hit
                    self.board[y][x - 1] = ShipStatus.Miss if self.board[y][x - 1] != ShipStatus.Hit else ShipStatus.Hit
                    self.board[y][x + 1] = ShipStatus.Miss if self.board[y][x + 1] != ShipStatus.Hit else ShipStatus.Hit

    def _was_shot_before(self, x, y):
        return self.board[y][x] == ShipStatus.Hit or self.board[y][x] == ShipStatus.Miss


class Conector():
    # obsluguje polaczenie z API serwera przekazujacymi rozgrywke
    def __init__(self):
        self.url = "http://127.0.0.1:8000/"

    def get_list_of_available_games(self):
        response = requests.get(url=self.url + "statki/getListOfGames")
        return json.loads(response.content.decode('utf8'))['dostepneGry']

    def start_new_game(self, nazwaGry, playerName):
        gameData = {
            'gameName': str(nazwaGry),
            'playerName': str(playerName)
        }
        response = requests.post(self.url + "statki/startNewGame", json=gameData)
        return json.loads(response.content.decode('utf8'))

    def joinGame(self, nazwaGry, playerName):
        gameData = {
            'gameName': str(nazwaGry),
            'playerName': str(playerName)
        }
        response = requests.post(self.url + "statki/joinGame", json=gameData)
        return json.loads(response.content.decode('utf8'))

    def sendOwnBoard(self, plansza, gameID, playerID):
        gameData = {
            'board': json.dumps(plansza),
            'gameName': gameID,
            'playerName': playerID
        }
        response = requests.post(self.url + "statki/getPlayerBoard", json=gameData)
        return response.content

    def getEnemyBoard(self, gameID, playerID):
        gameData = {
            'gameName': gameID,
            'playerName': playerID
        }
        response = requests.post(self.url + "statki/getEnemyBoard", json=gameData)
        return response.content.decode("utf-8")

    def sendShot(self, x, y, gameID, playerID):
        gameData = {
            'xShot': x,
            'yShot': y,
            'gameID': gameID,
            'playerID': playerID
        }
        response = requests.post(self.url + "/sendShot", json=json.dumps(gameData))
        return response.content

    def checkIfGameIsReady(self, nazwaGry, playerName):
        gameData = {
            'gameName': nazwaGry,
        }
        response = requests.post(self.url + "statki/checkForSecondPlayer", json=gameData)
        return json.loads(response.content.decode('utf8'))

    def checkTurn(self, nazwaGry, playerName):
        gameData = {
            'gameName': str(nazwaGry),
            'playerName': str(playerName)
        }
        response = requests.post(self.url + "statki/checkTurn", json=gameData)
        return json.loads(response.content.decode('utf8'))["mojaTura"]

    def endTurn(self, nazwaGry, playerName):
        gameData = {
            'gameName': str(nazwaGry),
            'playerName': str(playerName)
        }
        response = requests.post(self.url + "statki/endTurn", json=gameData)

    def checkGameResult(self, nazwaGry, playerName):
        gameData = {
            'gameName': str(nazwaGry),
            'playerName': str(playerName)
        }
        response = requests.post(self.url + "statki/sprawdzenieWynikuGry", json=gameData)
        return json.loads(response.content.decode('utf8'))

    def uploadGameResult(self, nazwaGry, playerName):
        gameData = {
            'gameName': str(nazwaGry),
            'playerName': str(playerName),
        }
        response = requests.post(self.url + "statki/przeslanieWynikuGry", json=gameData)


class GameControler():
    def __init__(self):
        self.player_name = ""
        self.game_name = ""
        self.set_player_name()
        self.run_game_menu()
        self.gameLoop()

    def _getEnemyBoard(self):
        return createBoardFromJson(self.connector.getEnemyBoard(self.game_name, self.player_name))

    def set_player_name(self):
        while True:
            input_name = input('Wpisz swoje imie:')
            if input_name:
                self.player_name = input_name
                print('Witaj, ' + self.player_name)
                break
            else:
                print('Podane imie jest nieprawidlowe')

    def run_game_menu(self):
        options = {
            1: self._join_to_game,
            2: self._create_new_game,
            3: self._exit_game
        }
        while True:
            input_value = input(
                "Wybierz co chcesz zrobic wpisujac odpowiednia cyfrę:\n1.Wyświetle otwarte gry\n2.Hostuj nową grę\n3.Wyjście")
            try:
                option_choosed = int(input_value)
                options[option_choosed]()
                break
            except ValueError:
                print("Wpisano niepoprawną wartość. Wpisz wartość ponownie.")

    def _exit_game(self):
        print("Zapraszamy ponownie!")
        sys.exit()

    def _create_new_game(self):
        self.connector = Conector()
        while True:
            input_value = input("Podaj nazwę nowej gry: ")
            if input_value:
                self.game_name = input_value
                server_response = self.connector.start_new_game(self.game_name, self.player_name)
                self.player_board = PlayerBoard()
                self.connector.sendOwnBoard(self.player_board.board, self.game_name, self.player_name)
                self._wait_for_second_player()

                break
            print("Nie wpisano nazwy gry. Prosze podac poprawna nazwę gry!")

    def _wait_for_second_player(self):
        while True:
            try:
                time.sleep(1)
                result = self.connector.checkIfGameIsReady(self.game_name, self.player_name)
                if result["message"] == "ready":
                    print("Gracz 2 ukonczyl plansze")
                    break
                else:
                    print(result["message"])

            except Exception as e:
                print("Błąd w czasie oczekiwania na 2 gracza: ", e)

    def _join_to_game(self):
        self.connector = Conector()
        available_games = self.connector.get_list_of_available_games()
        if available_games:
            message = "Dostępne gry:\n"
            for available_game in available_games:
                message += "{INDEX}: nazwa gry: {GAME_NAME}, przeciwnik: {ENEMY_NAME}\n".format(
                    INDEX=available_games.index(available_game),
                    GAME_NAME=available_game["game_name"],
                    ENEMY_NAME=available_game["enemy"]
                )
            print(message)
            self._chose_the_game(available_games)
        else:
            print("Brak dostępnych gier na serwerze. Proszę spróbować później")

    def _chose_the_game(self, available_games):
        while True:
            game_number = ''
            try:
                game_number = int(input("Prosze wprowadzic numer gry do której chcesz dołączyć: "))
            except ValueError:
                print("Podano błędną wartość liczbową. Podaj numer gry do której chcesz dołączyć")
            if game_number > len(available_games) - 1:
                print("Podano ID gry nie będące na liście")
            else:
                self.game_name = available_games[game_number]['game_name']
                break
        a = self.connector.joinGame(self.game_name, self.player_name)
        self.player_board = PlayerBoard()
        self.connector.sendOwnBoard(self.player_board.board, self.game_name, self.player_name)

    def gameLoop(self):
        self.player_board.board = self._getEnemyBoard()
        while True:
            while True:
                time.sleep(1)
                if self.connector.checkGameResult(self.game_name, self.player_name)[
                    'wynikGry'] != "graNierozstrzygnieta":
                    print(self.connector.checkGameResult(self.game_name, self.player_name)['wynikGry'])
                    print("Zapraszamy ponownie!")
                    sys.exit()

                if self.connector.checkTurn(self.game_name, self.player_name):
                    break
                else:
                    print("Oczekiwanie na zakończenie ruchu przez przeciwnika")
            self.player_board._players_turn(self.game_name, self.player_name, self.connector)

            self.connector.endTurn(self.game_name, self.player_name)


a = GameControler()