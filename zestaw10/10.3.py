import stosBezPowtorzen as lib
import unittest


class FracModuleTest(unittest.TestCase):
    def setUp(self):
        self.stackObj1 = lib.Stack(100)
        self.stackObj1.add(3)
        self.stackObj1.add(4)
        self.stackObj1.add(5)
        self.stackObj2 = lib.Stack(100)

    def test_add(self):
        self.stackObj1.add(3)
        self.assertEqual(self.stackObj1.stack, [3, 4, 5])
        self.stackObj1.add(6)
        self.assertEqual(self.stackObj1.stack, [3, 4, 5, 6])

    def test_remove(self):
        self.assertEqual(self.stackObj1.remove(), 5)
        self.assertEqual(self.stackObj1.remove(), 4)

    def test_isEmpty(self):
        self.assertTrue(self.stackObj2.isEmpty())
        self.assertFalse(self.stackObj1.isEmpty())

    def test_str(self):
        self.assertEqual(str(self.stackObj1), "[3, 4, 5]")
        self.assertEqual(str(self.stackObj2), "[]")


if __name__ == '__main__':
    unittest.main()
