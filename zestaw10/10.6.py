import kolejkaPriorytetowa as lib
import unittest


class FracModuleTest(unittest.TestCase):
    def setUp(self):
        self.queueObj1 = lib.PriorityQueue()
        self.queueObj2 = lib.PriorityQueue()
        self.queueObj1.add(5, 1)
        self.queueObj1.add(4, 2)
        self.queueObj1.add(3, 3)
        self.queueObj1.add(2, 4)
        self.queueObj1.add(6, 10)

    def test_add(self):
        self.assertEqual(self.queueObj1.queue, [[2, 4], [3, 3], [4, 2], [5, 1], [6, 10]])
        self.queueObj1.add(20,20)
        self.assertEqual(self.queueObj1.queue, [[2, 4], [3, 3], [4, 2], [5, 1], [6, 10], [20, 20]])

    def test_remove(self):
        self.assertEqual(self.queueObj1.remove(), [6, 10])
        self.assertEqual(self.queueObj1.remove(), [5, 1])

    def test_isEmpty(self):
        self.assertTrue(self.queueObj2.isEmpty())
        self.assertFalse(self.queueObj1.isEmpty())

    def test_str(self):
        self.assertEqual(str(self.queueObj1), "[[2, 4], [3, 3], [4, 2], [5, 1], [6, 10]]")
        self.assertEqual(str(self.queueObj2), "[]")

    def test_increase(self):
        self.assertEqual(str(self.queueObj1), "[[2, 4], [3, 3], [4, 2], [5, 1], [6, 10]]")
        self.queueObj1.increase(2)
        self.assertEqual(str(self.queueObj1), "[[4, 4], [5, 3], [6, 2], [7, 1], [8, 10]]")




if __name__ == '__main__':
    unittest.main()
