import kolejkaLosowa as lib
import unittest


class FracModuleTest(unittest.TestCase):
    def setUp(self):
        self.queueObj1 = lib.RandomQueue()
        self.queueObj2 = lib.RandomQueue()
        self.queueObj1.insert(1)
        self.queueObj1.insert(2)
        self.queueObj1.insert(3)
        self.queueObj1.insert(4)
        self.queueObj1.insert(5)

    def test_add(self):
        self.assertEqual(self.queueObj1.data, {0: 1, 1: 2, 2: 3, 3: 4, 4: 5})
        self.queueObj1.insert(6)
        self.assertEqual(self.queueObj1.data, {0: 1, 1: 2, 2: 3, 3: 4, 4: 5, 5: 6})

    def test_remove(self):
        self.assertTrue(self.queueObj1.remove() in [1, 2, 3, 4, 5])

    def test_isEmpty(self):
        self.assertTrue(self.queueObj2.is_empty())
        self.assertFalse(self.queueObj1.is_empty())

    def test_str(self):
        self.assertEqual(str(self.queueObj1), "[1, 2, 3, 4, 5]")
        self.assertEqual(str(self.queueObj2), "[]")

    def test_increase(self):
        self.assertEqual(str(self.queueObj1), "[1, 2, 3, 4, 5]")
        self.queueObj1.clear()
        self.assertEqual(str(self.queueObj1), "[]")


if __name__ == '__main__':
    unittest.main()
