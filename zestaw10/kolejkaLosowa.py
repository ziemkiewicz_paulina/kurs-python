import random as r


class RandomQueue:
    __slots__ = {'data'}

    def __init__(self):
        self.data = {}

    def insert(self, newValue):
        newIndex = len(self.data)
        self.data[newIndex] = newValue

    def remove(self):
        if len(self.data) == 0:
            raise ValueError
        if len(self.data) != 1:
            randomKey = r.randrange(0, len(self.data) - 1)
        else:
            randomKey = 0
        print(randomKey)
        value = self.data[randomKey]
        if randomKey != len(self.data):
            self.data[randomKey] = self.data[len(self.data) - 1]
            del self.data[len(self.data) - 1]
        else:
            del self.data[randomKey]
        return value

    def is_empty(self):
        return len(self.data) == 0

    def is_full(self):
        return False

    def clear(self):
        self.data = {}

    def __str__(self):
        listOfValues = []
        for key, values in self.data.items():
            listOfValues.append(values)
        return str(listOfValues)


a = RandomQueue()
a.insert(1)
a.insert(2)
a.insert(3)
a.insert(4)
a.insert(5)
print(a.data)

