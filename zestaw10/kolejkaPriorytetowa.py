class PriorityQueue:
    def __init__(self):
        self.queue = []

    def add(self, priority, value):
        self.queue.append([priority, value])
        self.queue.sort()

    def printQueue(self):
        print(self.queue)

    def __str__(self):
        return str(self.queue)

    def remove(self):
        return self.queue.pop()

    def isEmpty(self):
        return len(self.queue) == 0

    def increase(self, value):
        tempList = []
        for element in self.queue:
            element[0] += value
            tempList.append(element)
        self.queue = tempList

a = PriorityQueue()
a.add(5,1)
a.add(4,2)
a.add(3,3)
a.add(2,4)
a.add(6, 10)
print(a)
a.increase(2)
print(a)