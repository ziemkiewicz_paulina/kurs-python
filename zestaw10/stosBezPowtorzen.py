class Stack:
    def __init__(self, maxValue):
        self.maxValue = maxValue
        self.stack = []
        self.values = []
        for i in range(maxValue):
            self.values.append(0)

    def add(self, addedValue):
        if addedValue >= self.maxValue or addedValue < 0 or not isinstance(addedValue, int):
            raise ValueError
        if self.values[addedValue] == 1:
            pass
        else:
            self.values[addedValue] = 1
            self.stack.append(addedValue)

    def remove(self):
        popedValue = self.stack.pop()
        self.values[popedValue] = 0
        return popedValue

    def isEmpty(self):
        return len(self.stack) == 0

    def printStack(self):
        print(self.stack)

    def __str__(self):
        return str(self.stack)

a = Stack(100)
a.add(3)
a.add(4)
a.add(5)
print(a.remove())