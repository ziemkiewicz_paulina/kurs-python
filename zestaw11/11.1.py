from createRandomData import *

print('a) różne liczby całkowite od 0 do N-1 w kolejności losowej')
print(create_ramdom_int_list(10, 10))

print('b) różne liczby int od 0 do N-1 prawie posortowane')
print(create_nearly_sorted_int_list(10000, 10))

print('c) różne liczby int od 0 do N-1 prawie posortowane w odwrotnej kolejności')
print(create_nearly_sorted_int_list_reversed(10000, 10))

print('d) N liczb float w kolejności losowej o rozkładzie gaussowskim')
print(gaussian_random_list(1, 60))

print('e) N liczb int w kolejności losowej, o wartościach powtarzających się, należących do zbioru k elementowego')
print(create_random_list_with_set_elements(10, 50))
