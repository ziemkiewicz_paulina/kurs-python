def bubbleSort(List):
    listLength = len(List)
    while 1:
        changeFlag = False
        for i in range(listLength - 1):
            if List[i] > List[i + 1]:
                changeFlag = True
                List[i], List[i + 1] = List[i + 1], List[i]
        if changeFlag == False:
            break
    return List