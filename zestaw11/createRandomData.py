import numpy as np
import random
import math

def create_ramdom_int_list(maxInt, listLen):
    return np.random.randint(0, maxInt, listLen)


def create_nearly_sorted_int_list(maxInt, listLen):
    x = maxInt * 0.1
    ints_list = []
    sorted_list = [np.random.randint(0, maxInt) for i in range(listLen)]
    sorted_list.sort()
    print(sorted_list)
    for element in sorted_list:
        ints_list.append(math.fabs(element + int(random.uniform(-x, x))))
    print(ints_list)
    return ints_list


def create_nearly_sorted_int_list_reversed(maxInt, listLen):
    return create_nearly_sorted_int_list(maxInt, listLen).reverse()


def gaussian_random_list(maxInt, listLen):
    return np.random.randn(maxInt, listLen)


def create_random_list_with_set_elements(k, listLen):
    ints_List = []
    for i in range(listLen):
        ints_List.append(np.random.randint(0, k))
    return ints_List
