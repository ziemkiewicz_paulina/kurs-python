def insertionSort(Lista):
    for i in range(1, len(Lista)):
        j = i - 1
        element = Lista[i]

        while (Lista[j] > element) and (j >= 0):
            Lista[j + 1] = Lista[j]
            j = j - 1
        Lista[j + 1] = element
