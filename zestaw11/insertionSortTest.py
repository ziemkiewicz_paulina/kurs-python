import unittest
from insertionSortTest import insertionSort


class TestInsertSort(unittest.TestCase):
    def setUp(self):
        self.table1 = [1, 2, 3, 4, 5, 0]
        self.table1_sorted = [0, 1, 2, 3, 4, 5]
        self.table2 = [7, 4, 2, 9, 71, 8, 2, 0, 2]
        self.table2_sorted = [0, 2, 2, 2, 4, 7, 8, 9, 71]
        self.table3 = [8, 2, 4, 7, 9, 2, 4, 7, 8, 3, 5]
        self.table3_sorted = [2, 2, 3, 4, 4, 5, 7, 7, 8, 8, 9]

    def test_bubble_sort(self):
        self.assertTrue(insertionSort(self.table1) == self.table1_sorted)
        self.assertTrue(insertionSort(self.table2) == self.table2_sorted)
        self.assertTrue(insertionSort(self.table3) == self.table3_sorted)


if __name__ == '__main__':
    unittest.main()
