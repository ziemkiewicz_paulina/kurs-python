print('12.3 znajdowanie mediany')


def median(lst):
    sortedLst = sorted(lst)
    lstLen = len(lst)
    index = (lstLen - 1) // 2

    if (lstLen % 2):
        return sortedLst[index]
    else:
        return (sortedLst[index] + sortedLst[index + 1]) / 2.0


print([2, 0, 46, 9, 4, 8, -1])
print('mediana:', median([2, 0, 46, 9, 4, 8, -1]))

print('12.4 znajdowanie dominanty')


def mode(lst):
    sortedLst = sorted(lst)
    set(lst)
    return max(set(lst), key=lst.count)


print([0, 0, 1, 3, 6, 4])
print('moda:', mode([0, 0, 1, 3, 6, 4]))
