class RuchySkoczka():
    def __init__(self):
        self.N = 6
        self.RUCHY_SKOCZKA = 8
        self.delta_x = [2, 1, -1, -2, -2, -1, 1, 2]
        self.delta_y = [1, 2, 2, 1, -1, -2, -2, -1]
        self.plansza = {}
        for i in range(self.N):
            for j in range(self.N):
                self.plansza[i, j] = 0

    def rysuj(self):
        for i in range(self.N):
            linia = ""
            for j in range(self.N):
               linia += str(self.plansza[i, j]) + " "
            print(linia)

    def dopuszczalny(self, x, y):
        return 0 <= x < self.N and 0 <= y < self.N and self.plansza[x, y] == 0

    def zapisz(self, krok, x, y):
        self.plansza[x, y] = krok

    def wymaz(self, x, y):
        self.plansza[x, y] = 0

    def probuj(self, krok, x, y):
        udany = False
        kandydat = 0
        while (not udany) and (kandydat < self.RUCHY_SKOCZKA):
            print("kandydat:", kandydat)
            u = x + self.delta_x[kandydat]
            v = y + self.delta_y[kandydat]
            if self.dopuszczalny(u, v):
                self.zapisz(krok, u, v)
                if krok < self.N * self.N:
                    udany = self.probuj(krok + 1, u, v)
                    if not udany:
                        self.wymaz(u, v)
                else:
                    udany = True
            kandydat += 1
        return udany




a = RuchySkoczka()

a.zapisz(1, 0, 2)

if a.probuj(2, 0, 0):
    print("Mamy rozwiązanie")
    a.rysuj()
else:
    print("Nie istnieje rozwiązanie")
