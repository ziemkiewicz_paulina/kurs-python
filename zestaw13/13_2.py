class ProblemOsmiuHetmanow():
    def __init__(self, rozmiarPlanszy):
        self.rozmiarPlanszy = rozmiarPlanszy
        self.iloscRozwiazan = 0
        self.rozwiaz()

    def rozwiaz(self):
        poozycje = [-1] * self.rozmiarPlanszy
        self.umiescHetmana(poozycje, 0)
        print("Znaleziono ", self.iloscRozwiazan, " rozwiązań")

    def umiescHetmana(self, pozycje, wiersz):
        if wiersz == self.rozmiarPlanszy:
            self.wypiszRozwiazanie(pozycje)
            self.iloscRozwiazan += 1
        else:
            for kolumna in range(self.rozmiarPlanszy):
                if self.sprawdzPoprawnoscPozycji(pozycje, wiersz, kolumna):
                    pozycje[wiersz] = kolumna
                    self.umiescHetmana(pozycje, wiersz + 1)

    def sprawdzPoprawnoscPozycji(self, pozycje, zajeteWiersze, kolumna):
        for i in range(zajeteWiersze):
            if pozycje[i] == kolumna or \
                    pozycje[i] - i == kolumna - zajeteWiersze or \
                    pozycje[i] + i == kolumna + zajeteWiersze:
                return False
        return True

    def wypiszRozwiazanie(self, pozycje):
        for wiersz in range(self.rozmiarPlanszy):
            pojedynczyWiersz = ""
            for kolumna in range(self.rozmiarPlanszy):
                if pozycje[wiersz] == kolumna:
                    pojedynczyWiersz += "Q "
                else:
                    pojedynczyWiersz += ". "
            print(pojedynczyWiersz)
        print("\n")


ProblemOsmiuHetmanow(8)
