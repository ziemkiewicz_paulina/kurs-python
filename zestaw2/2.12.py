line = 'Today I consider myself the luckiest man on the face of the earth'
list_of_words = line.split()
first_letters = [word[0] for word in list_of_words]
first_word = "".join(first_letters)
print(first_word.lower())

last_letters = [word[-1] for word in list_of_words]
second_word = "".join(last_letters)
print(second_word.lower())
