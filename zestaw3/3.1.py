x = 2 ; y = 3 ;# Średnik nie robi nic w wyświetlanym kodzie. Nie powinno się ich używać, gdyż jest to niepoprawne składniowo.
if (x > y):
    result = x;
else:
    result = y;

for i in "qwerty": if ord(i) < 100: print i # funkcja print wymaga użycia nawiasów okrągłych
for i in "axby": print ord(i) if ord(i) < 100 else i
