numbers = ["MMCXIX"]

def roman2int(stringWithRomanNumber):
    intNumber = 0
    while True:
        value, romanLen = fitSingleNumber(stringWithRomanNumber)
        print(stringWithRomanNumber)
        intNumber += value
        stringWithRomanNumber = stringWithRomanNumber[romanLen:]
        if romanLen == 0:
            break
    return intNumber

def fitSingleNumber(number):
    dictionaryWithTranslations = {
        'M': 1000,
        'MM': 2000,
        'MMM': 3000,

        'C': 100,
        'CC': 200,
        'CCC': 300,
        'CD': 400,
        'D': 500,
        'DC': 600,
        'DCC': 700,
        'DCCC': 800,
        'CM': 900,

        'X': 10,
        'XX': 20,
        'XXX': 30,
        'XL': 40,
        'L': 50,
        'LX': 60,
        'LXX': 70,
        'LXXX': 80,
        'XC': 90,

        'I': 1,
        'II': 2,
        'III': 3,
        'IV': 4,
        'V': 5,
        'VI': 6,
        'VII': 7,
        'VIII': 8,
        'IX': 9,
    }
    try:
        return dictionaryWithTranslations[number[0:4]], 4
    except:
        try:
            return dictionaryWithTranslations[number[0:3]], 3
        except:
            try:
                return dictionaryWithTranslations[number[0:2]], 2
            except:
                try:
                    if len(number) == 0:
                        return 0, 0
                    else:
                        return dictionaryWithTranslations[number[0]], 1
                except:
                    return 0

print(roman2int('MMMDLXVIII'))