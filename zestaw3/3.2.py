L = [3, 5, 4] ; L = L.sort() #ok
    x, y = 1, 2, 3 #trzy wartości nie mogą zostać przypisane 2 zmiennym
X = 1, 2, 3 ; X[1] = 4  # X nie jest definiowane jako lista
X = [1, 2, 3] ; X[3] = 4 #lista iterowana jest od 0, w liście nie ma wartości X[3]
X = "abc" ; X.append("d") #metoda append nie może być użyta na stringu tylko dodaje elementy na końcu listy
map(pow, range(8))#funkcja pow potrzebuje tylko 2 argumentów - pow(__x,__y)
