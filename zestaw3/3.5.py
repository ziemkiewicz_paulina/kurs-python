length = 15
pattern = "|...."
linear = [' ', '0']
for i in range(length):
    linear[0] += pattern
    linear[1] += "%5s" % (i + 1)
print(linear[0] + '|', '\n', linear[1])