frame_vertically = "+---"
frame_horizontally = "|   "
Size_vertically = 4
Size_horizontally = 2
board = ''

def generate_array(pattern, end_cap):
    board = ''
    for j in range(Size_vertically):
        board += pattern
        if j == Size_vertically - 1:
            board += end_cap + '\n'
    return board

for _ in range(Size_horizontally):
    board += generate_array(frame_vertically, '+')
    board += generate_array(frame_horizontally, '|')
board += generate_array(frame_vertically, '+')
print(board)
