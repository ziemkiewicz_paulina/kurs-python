a = [[4, 7, 9], (14, 2), [3, 4], (5, 6, 7)]
b = [(0, 5, 8), (3, 4), (5, 13, 7), [1,87, 5, 6, 7]]

new_a = set()
new_b = set()

for el in a:
    new_a.update(set(el))

for el in b:
    new_b.update(set(el))

print('List of elements occurring simultaneously in both sequences', new_a.intersection(new_b))
print('List of all elements from both sequences', new_a.union(new_b))
