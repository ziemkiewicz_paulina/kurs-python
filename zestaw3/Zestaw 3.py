'''
'Zadanie 3.1'
x = 2 ; y = 3 ;# Średnik nie robi nic w wyświetlanym kodzie. Nie powinno się ich używać, gdyż jest to niepoprawne składniowo.
if (x > y):
    result = x;
else:
    result = y;

for i in "qwerty": if ord(i) < 100: print i # funkcja print wymaga użycia nawiasów okrągłych
for i in "axby": print ord(i) if ord(i) < 100 else i



'Zadanie 3.2'
L = [3, 5, 4] ; L = L.sort() #ok
    x, y = 1, 2, 3 #trzy wartości nie mogą zostać przypisane 2 zmiennym
X = 1, 2, 3 ; X[1] = 4  # X nie jest definiowane jako lista
X = [1, 2, 3] ; X[3] = 4 #lista iterowana jest od 0, w liście nie ma wartości X[3]
X = "abc" ; X.append("d") #metoda append nie może być użyta na stringu tylko dodaje elementy na końcu listy
map(pow, range(8))#funkcja pow potrzebuje tylko 2 argumentów - pow(__x,__y)L = [3, 5, 4] ; L = L.sort() #ok
    x, y = 1, 2, 3 #trzy wartości nie mogą zostać przypisane 2 zmiennym
X = 1, 2, 3 ; X[1] = 4  # X nie jest definiowane jako lista
X = [1, 2, 3] ; X[3] = 4 #lista iterowana jest od 0, w liście nie ma wartości X[3]
X = "abc" ; X.append("d") #metoda append nie może być użyta na stringu tylko dodaje elementy na końcu listy
map(pow, range(8))#funkcja pow potrzebuje tylko 2 argumentów - pow(__x,__y)

'''



'Zadanie 3.3'
answer=[]
for value in range(31):
    if value % 3 != 0:
        answer.append(value)
print('Zadanie 3.3:',answer)



'Zadanie 3.4'
print('Zadanie 3.4:')
while True:
    value = (input("Give a number: "))
    if value == "stop":
        break
    elif not value.isdigit():
        print ("This is not a number!")
    else:
        print ('x=',value,'The third exponential of x is:',int(value) ** 3)



'Zadanie 3.5'
length = 15
pattern = "|...."
linear = [' ', '0']
for i in range(length):
    linear[0] += pattern
    linear[1] += "%5s" % (i + 1)
print('Zadanie 3.5:')
print(linear[0] + '|', '\n', linear[1])



'Zadanie 3.6'
frame_vertically = "+---"
frame_horizontally = "|   "
Size_vertically = 4
Size_horizontally = 2
board = ''

def generate_array(pattern, end_cap):
    board = ''
    for j in range(Size_vertically):
        board += pattern
        if j == Size_vertically - 1:
            board += end_cap + '\n'
    return board

for _ in range(Size_horizontally):
    board += generate_array(frame_vertically, '+')
    board += generate_array(frame_horizontally, '|')
board += generate_array(frame_vertically, '+')
print('Zadanie 3.6:')
print(board)



'Zadanie 3.8'
a = [[4, 7, 9], (14, 2), [3, 4], (5, 6, 7)]
b = [(0, 5, 8), (3, 4), (5, 13, 7), [1,87, 5, 6, 7]]

new_a = set()
new_b = set()

for el in a:
    new_a.update(set(el))

for el in b:
    new_b.update(set(el))
print('Zadanie 3.8:')
print('List of elements occurring simultaneously in both sequences', new_a.intersection(new_b))
print('List of all elements from both sequences', new_a.union(new_b))



'Zadanie 3.9'
sequence_list = [[], [4, 5, 9], (13, 2), [3, 4], (5, 6, 7)]
result=(list(map(lambda x: sum(x), sequence_list)))
print('Zadanie 3.9:')
print(result)



'Zadanie 3.10'
numbers = ["MMCXIX"]

def roman2int(stringWithRomanNumber):
    intNumber = 0
    while True:
        value, romanLen = fitSingleNumber(stringWithRomanNumber)
        intNumber += value
        stringWithRomanNumber = stringWithRomanNumber[romanLen:]
        if romanLen == 0:
            break
    return intNumber

def fitSingleNumber(number):
    dictionaryWithTranslations = {
        'M': 1000,
        'MM': 2000,
        'MMM': 3000,

        'C': 100,
        'CC': 200,
        'CCC': 300,
        'CD': 400,
        'D': 500,
        'DC': 600,
        'DCC': 700,
        'DCCC': 800,
        'CM': 900,

        'X': 10,
        'XX': 20,
        'XXX': 30,
        'XL': 40,
        'L': 50,
        'LX': 60,
        'LXX': 70,
        'LXXX': 80,
        'XC': 90,

        'I': 1,
        'II': 2,
        'III': 3,
        'IV': 4,
        'V': 5,
        'VI': 6,
        'VII': 7,
        'VIII': 8,
        'IX': 9,
    }
    try:
        return dictionaryWithTranslations[number[0:4]], 4
    except:
        try:
            return dictionaryWithTranslations[number[0:3]], 3
        except:
            try:
                return dictionaryWithTranslations[number[0:2]], 2
            except:
                try:
                    if len(number) == 0:
                        return 0, 0
                    else:
                        return dictionaryWithTranslations[number[0]], 1
                except:
                    return 0

print('Zadanie 3.9:')
print(roman2int('MMMDLXVIII'))