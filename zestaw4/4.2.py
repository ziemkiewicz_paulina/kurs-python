print('Zadanie 4.2 cz.1')
def get_linear(length=0):
    pattern = "....|"
    linear = ['|', '0']
    linear[0] += pattern * (length)
    for i in range(0, length):
        linear[1] += "%5s" % (i+1)
    return str.join('\n', (linear[0], linear[1]))
length=15
print(get_linear(length))




print('Zadanie 4.2 cz.2')

def generate_array(pattern, end_cap, Size_vertically):
    board = ''
    for j in range(Size_vertically):
        board += pattern
        if j == Size_vertically - 1:
            board += end_cap + '\n'
    return board

def generateArray(Size_horizontally,Size_vertically ):
    frame_vertically = "+---"
    frame_horizontally = "|   "
    board = ''

    for _ in range(Size_horizontally):
        board += generate_array(frame_vertically, '+', Size_vertically)
        board += generate_array(frame_horizontally, '|', Size_vertically)
    board += generate_array(frame_vertically, '+', Size_vertically)
    return str(board)

print(generateArray(2,3))

