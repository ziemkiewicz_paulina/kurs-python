#print('Zadanie 4.4')
number=6
def fibonacciRecurency(n):
    if n < 0:
        print("Please enter a non-negative number")
    elif n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibonacciRecurency(n - 1) + fibonacciRecurency(n - 2)

print(number,'-th expression of the Fibonacci sequence is:',fibonacciRecurency(number))

print('Zadanie 4.4')
def fibonacciIterable(n):

    a, b = 0, 1
    for i in range(n):
        a, b = b, a + b
    return a
n=6
print(fibonacciIterable(n))