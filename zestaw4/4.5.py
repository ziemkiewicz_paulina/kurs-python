print('Zadanie 4.5')


def replaceRecurency(L, left, right):
    if left < right:
        L[left], L[right] = L[right], L[left]
        return replaceRecurency(L, left + 1, right - 1)
    else:
        return L


def replaceIter(L, left, right):
    while True:
        if left < right:
            L[left], L[right] = L[right], L[left]
            left += 1
            right -= 1
        else:
            return L


L = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
print('ITER: Initial list:', L)
left = 1
right = 5
L = replaceIter(L, left, right)
print('ITER: Inverted list:', L)

L = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
print('RECU: Initial list:', L)
left = 1
right = 5
L = replaceRecurency(L, left, right)
print('RECU: Inverted list:', L)
