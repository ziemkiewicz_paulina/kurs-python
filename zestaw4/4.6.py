print('Zadanie 4.6')

def sum_seq(sequence):
    sum = 0
    for i in sequence:
        if isinstance(i, (list, tuple)):
            sum += sum_seq(i)
        else:
            sum += i
    return sum
sequence = [(1,2,3,4),5,(6,8),[10,12],1,(5, 3, 1)]
print('The sum of all elements is:',sum_seq(sequence))