print('Zadanie 4.7')

def flatten(sequence):
    new_list = []
    for i in sequence:
        if isinstance(i, (list, tuple)):
            new_list.extend(flatten(i))
        else:
            new_list.append(i)
    return new_list

sequence = [1,2,3,(4,6),[],[11,(1,6,7)],8,[9]]
print(flatten(sequence))