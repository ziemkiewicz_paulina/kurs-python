print('Zadanie 4.2 cz.1')
def get_linear(length=0):
    pattern = "....|"
    linear = ['|', '0']
    linear[0] += pattern * (length)
    for i in range(0, length):
        linear[1] += "%5s" % (i+1)
    return str.join('\n', (linear[0], linear[1]))
length=15
print(get_linear(length))




print('Zadanie 4.2 cz.2')
def generate_array(pattern, end_cap, Size_vertically):
    board = ''
    for j in range(Size_vertically):
        board += pattern
        if j == Size_vertically - 1:
            board += end_cap + '\n'
    return board

def generateArray(Size_horizontally,Size_vertically ):
    frame_vertically = "+---"
    frame_horizontally = "|   "
    board = ''

    for _ in range(Size_horizontally):
        board += generate_array(frame_vertically, '+', Size_vertically)
        board += generate_array(frame_horizontally, '|', Size_vertically)
    board += generate_array(frame_vertically, '+', Size_vertically)
    return str(board)

print(generateArray(2,3))



print('Zadanie 4.3')
def get_factorial(n):
    factorial = 1
    for i in range(1, n + 1):
        factorial = factorial * i
    return factorial
number=4
print('The factorial of',number, 'is:',get_factorial(number))



print('Zadanie 4.4')
def fibonacciIterable(n):

    a, b = 0, 1
    for i in range(n):
        a, b = b, a + b
    return a
n=6
print(fibonacciIterable(n))

print('Zadanie 4.5')


def replaceRecurency(L, left, right):
    if left < right:
        L[left], L[right] = L[right], L[left]
        return replaceRecurency(L, left + 1, right - 1)
    else:
        return L


def replaceIter(L, left, right):
    while True:
        if left < right:
            L[left], L[right] = L[right], L[left]
            left += 1
            right -= 1
        else:
            return L


L = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
print('ITER: Initial list:', L)
left = 1
right = 5
L = replaceIter(L, left, right)
print('ITER: Inverted list:', L)

L = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
print('RECU: Initial list:', L)
left = 1
right = 5
L = replaceRecurency(L, left, right)
print('RECU: Inverted list:', L)

print('Zadanie 4.6')

def sum_seq(sequence):
    sum = 0
    for i in sequence:
        if isinstance(i, (list, tuple)):
            sum += sum_seq(i)
        else:
            sum += i
    return sum
sequence = [(1,2,3,4),5,(6,8),[10,12],1,(5, 3, 1)]
print('The sum of all elements is:',sum_seq(sequence))

print('Zadanie 4.7')

def flatten(sequence):
    new_list = []
    for i in sequence:
        if isinstance(i, (list, tuple)):
            new_list.extend(flatten(i))
        else:
            new_list.append(i)
    return new_list

sequence = [1,2,3,(4,6),[],[11,(1,6,7)],8,[9]]
print(flatten(sequence))