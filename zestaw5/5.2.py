import unittest
from zestaw5.fracs import *
f1 = [1, 2]
f2 = [1, 3]

class TestFractions(unittest.TestCase):
    def setUp(self):
        self.zero = [0, 1]
    def test_add_frac(self):
        self.assertEqual(add_frac(f1, f2), [5, 6])
    def test_sub_frac(self):
        self.assertEqual(sub_frac(f1, f2), [1, 6])
    def test_mul_frac(self):
        self.assertEqual(mul_frac(f1, f2), [1, 6])
    def test_div_frac(self):
        self.assertEqual(div_frac(f1, f2), [3, 2])
    def test_is_positive(self):
        self.assertEqual(is_positive(f1), True)
    def test_is_zero(self):
        self.assertEqual(is_zero(f1), False)
    def test_cmp_frac(self):
        self.assertEqual(cmp_frac(f1, f2), 1)
    def test_frac2float(self):
        self.assertEqual(frac2float(f1), 0.5)

if __name__ == '__main__':
    unittest.main()


