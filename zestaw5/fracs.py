def NWD(a, b):
    while b != 0:
        c = a % b
        a = b
        b = c
    return a


def gcd(n, d):
    while n != d:
        if n > d:
            n = n - d
        else:
            d = d - n
    return n


def __init__(n, d):
    f1 = int(n / gcd(abs(n), abs(d)))
    f2 = int(d / gcd(abs(n), abs(d)))
    if f2 < 0:
        f2 = abs(f2)
        f1 = -1 * f1
    elif f2 == 0:
        raise ZeroDivisionError


def add_frac(f1, f2):
    return [f1[0] * f2[1] + f1[1] * f2[0], f1[1] * f2[1]]


def sub_frac(f1, f2):
    return [f1[0] * f2[1] - f1[1] * f2[0], f1[1] * f2[1]]


def mul_frac(f1, f2):
    return [f1[0] * f2[0], f1[1] * f2[1]]


def div_frac(f1, f2):
    return [f1[0] * f2[1], f1[1] * f2[0]]


def is_positive(f):
    return False if f[0] * f[1] < 0 else True


def is_zero(f):
    return True if f[0] == 0 else False


def cmp_frac(f1, f2):
    if f1[0] * f2[1] - f1[1] * f2[0] > 0:
        return 1
    elif f1[0] * f2[1] - f1[1] * f2[0] < 0:
        return -1
    else:
        return 0


def frac2float(f):
    return float(f[0] / f[1])
