def factorial(n):
    fact = 1
    for i in range(1, n + 1):
        fact = fact * i
    return fact

def fibonacci(n):

    a, b = 0, 1
    for i in range(n):
        a, b = b, a + b
    return a

