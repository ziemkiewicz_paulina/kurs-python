def NWD(a, b):
    while b != 0:
        c = a % b
        a = b
        b = c
    return a


class Frac(object):
    def __init__(self, licznik, mianownik):
        if mianownik == 0:
            raise ValueError("Mianownik nie moze byc 0")

        else:
            self.ulamek = [licznik / NWD(licznik, mianownik), mianownik / NWD(licznik, mianownik)]

        pass

    def __str__(self):
        if self.ulamek[0] == 0:
            return str(0)

        elif self.ulamek[0] % self.ulamek[1] == 0:
            return str(self.ulamek[0] / self.ulamek[1])

        else:
            return str(self.ulamek)

    def __repr__(self):
        return "frac(%s, %s)" % (self.ulamek[0], self.ulamek[1])

    def __add__(self, other):
        a = self.ulamek[0] * other.ulamek[1] + other.ulamek[0] * self.ulamek[1]
        b = other.ulamek[1] * self.ulamek[1]
        return Frac(a, b)

    def __sub__(self, other):
        a = self.ulamek[0] * other.ulamek[1] - other.ulamek[0] * self.ulamek[1]
        b = other.ulamek[1] * self.ulamek[1]
        return Frac(a, b)

    def __mul__(self, other):
        a = self.ulamek[0] * other.ulamek[0]
        b = other.ulamek[1] * self.ulamek[1]
        return Frac(a, b)

    def __truediv__(self, other):
        if other.ulamek[0] == 0:
            raise ValueError("Nie mozna dzielic przez 0")

        else:
            a = self.ulamek[0] * other.ulamek[1]
            b = self.ulamek[1] * other.ulamek[0]
            return Frac(a, b)

    def __floordiv__(self, other):
        if other.ulamek[0] == 0:
            raise ValueError("Nie mozna dzielic przez 0")

        else:
            a = self.ulamek[0] * other.ulamek[1]
            b = self.ulamek[1] * other.ulamek[0]
            return Frac(a, b)


    def __eq__(self, other):
        if isinstance(self.ulamek, list) and isinstance(other, (float, int)):
            a = float(self.ulamek[0]) / float(self.ulamek[1])
            b = float(other)
            if a == b:
                return 1
            else:
                return 0
        elif isinstance(self.ulamek, list) and isinstance(other.ulamek, list):
            a = float(self.ulamek[0] / self.ulamek[1])
            b = float(other.ulamek[0] / other.ulamek[1])

            if a == b:
                return 1
            else:
                return 0

        else:
            raise TypeError

    def __ne__(self, other):
        if isinstance(self.ulamek, list) and isinstance(other, (float, int)):
            a = float(self.ulamek[0]) / float(self.ulamek[1])
            b = float(other)
            if a != b:
                return 1
            else:
                return 0
        elif isinstance(self.ulamek, list) and isinstance(other.ulamek, list):
            a = float(self.ulamek[0] / self.ulamek[1])
            b = float(other.ulamek[0] / other.ulamek[1])

            if a != b:
                return 1
            else:
                return 0

        else:
            raise TypeError

    def __lt__(self, other):
        if isinstance(self.ulamek, list) and isinstance(other, (float, int)):
            a = float(self.ulamek[0]) / float(self.ulamek[1])
            b = float(other)
            if a < b:
                return 1
            else:
                return 0
        elif isinstance(self.ulamek, list) and isinstance(other.ulamek, list):
            a = float(self.ulamek[0] / self.ulamek[1])
            b = float(other.ulamek[0] / other.ulamek[1])

            if a < b:
                return 1
            else:
                return 0

        else:
            raise TypeError

    def __gt__(self, other):
        if isinstance(self.ulamek, list) and isinstance(other, (float, int)):
            a = float(self.ulamek[0]) / float(self.ulamek[1])
            b = float(other)
            if a > b:
                return 1
            else:
                return 0
        elif isinstance(self.ulamek, list) and isinstance(other.ulamek, list):
            a = float(self.ulamek[0] / self.ulamek[1])
            b = float(other.ulamek[0] / other.ulamek[1])

            if a > b:
                return 1
            else:
                return 0

        else:
            raise TypeError

    def __ge__(self, other):
        if isinstance(self.ulamek, list) and isinstance(other, (float, int)):
            a = float(self.ulamek[0]) / float(self.ulamek[1])
            b = float(other)
            if a >= b:
                return 1
            else:
                return 0
        elif isinstance(self.ulamek, list) and isinstance(other.ulamek, list):
            a = float(self.ulamek[0] / self.ulamek[1])
            b = float(other.ulamek[0] / other.ulamek[1])

            if a >= b:
                return 1
            else:
                return 0

        else:
            raise TypeError

    def __le__(self, other):
        if isinstance(self.ulamek, list) and isinstance(other, (float, int)):
            a = float(self.ulamek[0]) / float(self.ulamek[1])
            b = float(other)
            if a <= b:
                return 1
            else:
                return 0
        elif isinstance(self.ulamek, list) and isinstance(other.ulamek, list):
            a = float(self.ulamek[0] / self.ulamek[1])
            b = float(other.ulamek[0] / other.ulamek[1])

            if a <= b:
                return 1
            else:
                return 0

        else:
            raise TypeError

    def is_positive(self):
        if self.ulamek[0] / self.ulamek[1] > 0:
            return True
        else:
            return False

    def is_zero(self):
        if self.ulamek[0] == 0:
            return True
        else:
            return False

    def __float__(self):
        return float(self.ulamek[0]) / float(self.ulamek[1])

    def __invert__(self):
        if self.ulamek[0] == 0:
            raise ValueError
        else:
            return Frac(self.ulamek[1], self.ulamek[0])

    def __neg__(self):
        return Frac(self.ulamek[0] * -1, self.ulamek[1])

    def __pos__(self):
        return self
