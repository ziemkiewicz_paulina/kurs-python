from rectangle import Rectangle
from points import Point
import unittest


class FracModuleTest(unittest.TestCase):
    def setUp(self):
        self.RectangleA = Rectangle(-1, -1, 1, 1)
        self.RectangleB = Rectangle(0, 0, 2, 2)

    def test_str_rectangle(self):
        self.assertEqual(str(self.RectangleA), "[(-1.0, -1.0), (1.0, 1.0)]")
        self.assertEqual(str(self.RectangleB), "[(0.0, 0.0), (2.0, 2.0)]")

    def test_repr_rectangle(self):
        self.assertEqual(self.RectangleA.__repr__(), 'Rectangle(-1.0, -1.0, 1.0, 1.0)')
        self.assertEqual(self.RectangleB.__repr__(), 'Rectangle(0.0, 0.0, 2.0, 2.0)')

    def test_eq_rectangle(self):
        self.assertTrue(self.RectangleA == Rectangle(-1, -1, 1, 1))
        self.assertTrue(self.RectangleB == Rectangle(0, 0, 2, 2))

    def test_ne_rectangle(self):
        self.assertTrue(self.RectangleA != Rectangle(-2, -2, 2, 2))

    def test_center_rectangle(self):
        self.assertEqual(self.RectangleA.center(), Point(0, 0))
        self.assertEqual(self.RectangleB.center(), Point(1, 1))

    def test_area_rectangle(self):
        self.assertEqual(self.RectangleA.area(), 4.0)
        self.assertEqual(self.RectangleB.area(), 4.0)

    def test_move_rectangle(self):
        self.RectangleA.move(1, 1)
        self.assertTrue(str(self.RectangleA) == str(self.RectangleB))

    def test_cover_rectangle(self):
        self.assertAlmostEqual(Rectangle(-1, -1, 1, 1).cover(Rectangle(0, 0, 3, 3)), Rectangle(-1, -1, 3, 3))
        self.assertAlmostEqual(Rectangle(-2, -2, -1, -1).cover(Rectangle(0, 0, 3, 3)), Rectangle(-2, -2, 3, 3))

    def test_intersection_rectangle(self):
        self.assertTrue(Rectangle(-1, -1, 1, 1).intersection(Rectangle(0, 0, 3, 3)) == Rectangle(0, 0, 1, 1))
        self.assertTrue(Rectangle(-2, -2, -1, -1).intersection(Rectangle(0, 0, 3, 3)) == None)

    def tests_make4_rectangle(self):
        self.assertTrue(Rectangle(0, 0, 2, 2).make4() == [
            Rectangle(0, 0, 1, 1),
            Rectangle(0, 1, 1, 2),
            Rectangle(1, 1, 2, 2),
            Rectangle(1, 0, 2, 1)
        ])


if __name__ == '__main__':
    unittest.main()
