import unittest
from circles import *


class TestCircle(unittest.TestCase):
    def setUp(self):
        self.exa1 = Circle(2, 6, 11)
        self.exa2 = Circle(0, 0, 12)
        self.exa3 = Circle(3, 8, 7)
        self.exa4 = Circle(1,1,1)
        self.exa5 = Circle(-1,1,1)

    def test_equal(self):
        self.assertFalse(self.exa1 == self.exa2)
        self.assertFalse(self.exa2 == self.exa3)
        self.assertTrue(self.exa1 == Circle(2, 6, 11))
        self.assertTrue(self.exa2 == Circle(0, 0, 12))
        self.assertTrue(self.exa3 == Circle(3, 8, 7))

    def test_move(self):
        self.assertEqual(self.exa1.move(1, 2), Circle(3, 8, 11))
        self.assertEqual(self.exa2.move(0, 3), Circle(0, 3, 12))
        self.assertEqual(self.exa3.move(-1, -3), Circle(2, 5, 7))

    def test_area(self):
        self.assertEqual(self.exa1.area(), 379.94)
        self.assertEqual(self.exa2.area(), 452.16)
        self.assertEqual(self.exa3.area(), 153.86)

    def test_cover(self):
        self.assertEqual(self.exa4.cover(self.exa5), Circle(0,1,2))

if __name__ == '__main__':
    unittest.main()