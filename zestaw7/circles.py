import math


class Point:
    def __init__(self, x=0.0, y=0.0):
        self.x = x
        self.y = y

    def __eq__(self, other):
        if isinstance(other, Circle):
            return self.x == other.pt.x and self.y == other.pt.y
        else:
            return self.x == other.x and self.y == other.y

    def __add__(self, other):
        self.x += other.x
        self.y += other.y
        return Point(self.x, self.y)

    def __str__(self):
        return str(self.x) + ", " + str(self.y)


PI = 3.14


class Circle:

    def __init__(self, x=0.0, y=0.0, radius=1):
        if radius < 0:
            raise ValueError("Podany promien nie moze byc ujemny")
        self.pt = Point(float(x), float(y))
        self.radius = radius

    def __str__(self):
        return str(self.pt) + " " + str(self.radius)

    def __repr__(self):
        return "Circle ({0}, {1}, {2})".format(self.pt.x, self.pt.y, self.radius)

    def __eq__(self, other):
        return self.pt == other.pt and self.radius == other.radius

    def __ne__(self, other):
        return not self == other

    def area(self):
        return PI * self.radius ** 2

    def move(self, x, y):
        return self.pt + Point(x, y)

    def cover(self, other):
        D = math.sqrt((self.pt.x - other.pt.x) **2 + (self.pt.y - other.pt.y) **2)
        if self.radius < other.radius and D + self.radius < other.radius:
            return Circle(other.pt.x, other.pt.y, other.radius)
        elif self.radius > other.radius and D + other.radius < self.radius:
            return Circle(self.pt.x, self.pt.y, self.radius)

        newRadius = (D + self.radius + other.radius) / 2
        theta = 1 / 2 + (other.radius - self.radius) / (2 * D)
        newX = (1-theta) * self.pt.x + theta * other.pt.x
        newY = (1-theta) * self.pt.y + theta * other.pt.y

        return Circle(newX, newY, newRadius)
