import math
from points import Point


class Rectangle:
    """Klasa reprezentujaca prostokat na plaszczyznie."""

    def __init__(self, x1=0, y1=0, x2=0, y2=0):
        if x1 < x2 and y1 < y2:
            self.pt1 = Point(x1, y1)  # lewy dolny rog
            self.pt2 = Point(x2, y2)  # prawy gorny rog
        else:
            raise ValueError("Podano bledna kolejnosc parametrow")

    def __str__(self):  # "[(x1, y1), (x2, y2)]"
        return "[(%s, %s), (%s, %s)]" % (self.pt1.x, self.pt1.y, self.pt2.x, self.pt2.y)

    def __repr__(self):  # "Rectangle(x1, y1, x2, y2)"
        return 'Rectangle(%s, %s, %s, %s)' % (self.pt1.x, self.pt1.y, self.pt2.x, self.pt2.y)

    def __eq__(self, other):  # obsluga rect1 == rect2
        return self.pt1 == other.pt1 and self.pt2 == other.pt2

    def __ne__(self, other):  # obsluga rect1 != rect2
        return not self.__eq__(other)

    def center(self):  # zwraca srodek prostokata
        center = Point(self.pt1.x, self.pt1.y)
        center.move((self.pt2.x - self.pt1.x) / 2, (self.pt2.y - self.pt1.y) / 2)
        return center

    def area(self):  # pole powierzchni
        return math.fabs(self.pt1.x - self.pt2.x) * math.fabs(self.pt1.y - self.pt2.y)

    def move(self, x, y):  # przesuniecie o (x, y)
        self.pt1.move(x, y)
        self.pt2.move(x, y)

    def cover(self, other):
        if isinstance(other, Rectangle):
            maxX = max(self.pt2.x, other.pt2.x)
            minX = min(self.pt1.x, other.pt1.x)
            maxY = max(self.pt2.y, other.pt2.y)
            minY = min(self.pt1.y, other.pt1.y)
            return Rectangle(minX, minY, maxX, maxY)
        else:
            raise TypeError

    def intersection(self, other):
        try:
            return Rectangle(
                max(self.pt1.x, other.pt1.x),
                max(self.pt1.y, other.pt1.y),
                min(self.pt2.x, other.pt2.x),
                min(self.pt2.y, other.pt2.y)
            )
        except:
            return None

    def make4(self):
        return [Rectangle(self.pt1.x, self.pt1.y, self.center().x, self.center().y),
                Rectangle(self.pt1.x, self.center().y, self.center().x, self.pt2.y),
                Rectangle(self.center().x, self.center().y, self.pt2.x, self.pt2.y),
                Rectangle(self.center().x, self.pt1.y, self.pt2.x, self.center().y)]
