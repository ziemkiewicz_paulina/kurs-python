# a x + b y + c = 0

def solve1(a, b, c):
    if a == 0 and b == 0:
        if c == 0:
            raise ValueError('RownanieTożsamościowe')
        else:
            raise ValueError('RownanieSprzeczne')
    if a == 0:
        return "y = " + str(-c/b)
    elif b == 0:
        return "x = " + str(-c/a)
    else:
        return "y= -({0} * x + {2})/{1}\n  x= -({1} * y + {2})/{0} ".format(a,b,c)

print('Wartość to: \n ',solve1(0,0,0))
