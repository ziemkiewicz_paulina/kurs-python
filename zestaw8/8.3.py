from random import *
from math import sqrt

def calc_pi(total):
    inside = 0  # liczba trafień w koło
    for i in range(0, total):
        x2 = random()**2 # generowanie liczb
        y2 = random()**2
        if sqrt(x2 + y2) < 1.0:# warunek na trafienie w koło
            inside += 1
    return float(4*inside/total)

print("Pi wyznaczone dla total = 100:  ", calc_pi(100))
print("Pi wyznaczone dla total = 1000:  ", calc_pi(1000))
print("Pi wyznaczone dla total = 100000:  ", calc_pi(100000))
print("Pi wyznaczone dla total = 1000000000:  ", calc_pi(1000000000))