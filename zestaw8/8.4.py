from math import sqrt

inputText =(input("Podaj 3 boki trójkąta (oddzielone spacjami): "). split())
def heron(a, b, c):
    if (a + b) > c and (a + c) > b and (b + c) > a:#
        p = 0.5 * (a + b + c)  # obliczmy współczynnik wzoru Herona
        P = sqrt(p * (p - a) * (p - b) * (p - c)) # pole trójkąta
        return 'Pole trójkąta wynosi:',P
    else:
        raise ValueError('Podane boki tójkąta są nieprawidłowe! ')

print(heron (int(inputText[0]), int(inputText[1]), int(inputText[2])))