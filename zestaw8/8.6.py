import unittest

def rekurencyP(i,j):
    if i == 0 and j == 0:
        return 0.5
    if j == 0 and i > 0:
        return 0
    if i == 0 and j > 0:
        return 1
    if j > 0 and i > 0:
        return 0.5 * (rekurencyP(i - 1, j) + rekurencyP(i, j - 1))



def dynamicP(k,l):
    position = {} # tablica 2D w postaci słownika
    if k == 0 and l ==0:
        return 0.5
    for i in range(k+1):
        position[(i, 0)] = 0
    for i in range(l+1):
        position[(0, i)] = 1
    for i in range(1,k+1):
        for j in range(1,l+1):
            position[(i, j)] = 0.5 * (position[(i - 1, j)] + position[(i, j - 1)])
    return position[(k,l)]



class MyTestCase(unittest.TestCase):

    def test_recurencyP(self):
        self.assertEqual(rekurencyP(0, 0), 0.5)
        self.assertEqual(rekurencyP(3, 0), 0)
        self.assertEqual(rekurencyP(2, 3), 0.6875)

    def test_dynamicP(self):
        self.assertEqual(dynamicP(0, 0), 0.5)
        self.assertEqual(dynamicP(3, 0), 0)
        self.assertEqual(dynamicP(2, 3), 0.6875)


if __name__ == '__main__':
    unittest.main()