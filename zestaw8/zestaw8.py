print("Zadanie 8.1")
def solve1(a, b, c):
    if a == 0 and b == 0:
        if c == 0:
            raise ValueError('RownanieTożsamościowe')
        else:
            raise ValueError('RownanieSprzeczne')
    if a == 0:
        return "y = " + str(-c/b)
    elif b == 0:
        return "x = " + str(-c/a)
    else:
        return "y= -({0} * x + {2})/{1}\n  x= -({1} * y + {2})/{0} ".format(a,b,c)

print('Wartość to: \n ',solve1(0,3,5))




print("Zadanie 8.3")

from random import *
from math import sqrt

def calc_pi(total):
    inside = 0  # liczba trafień w koło
    for i in range(0, total):
        x2 = random()**2 # generowanie liczb
        y2 = random()**2
        if sqrt(x2 + y2) < 1.0:# warunek na trafienie w koło
            inside += 1
    return float(4*inside/total)

print("Pi wyznaczone dla total = 100:  ", calc_pi(100))
print("Pi wyznaczone dla total = 1000:  ", calc_pi(1000))
print("Pi wyznaczone dla total = 100000:  ", calc_pi(100000))
print("Pi wyznaczone dla total = 1000000000:  ", calc_pi(1000000000))




print("Zadanie 8.4")

from math import sqrt

inputText =(input("Podaj 3 boki trójkąta (oddzielone spacjami): "). split())
def heron(a, b, c):
    if (a + b) > c and (a + c) > b and (b + c) > a:#
        p = 0.5 * (a + b + c)  # obliczmy współczynnik wzoru Herona
        P = sqrt(p * (p - a) * (p - b) * (p - c)) # pole trójkąta
        return 'Pole trójkąta wynosi:',P
    else:
        raise ValueError('Podane boki tójkąta są nieprawidłowe! ')

print(heron (int(inputText[0]), int(inputText[1]), int(inputText[2])))




print("Zadanie8.6")

import unittest

def rekurencyP(i,j):
    if i == 0 and j == 0:
        return 0.5
    if j == 0 and i > 0:
        return 0
    if i == 0 and j > 0:
        return 1
    if j > 0 and i > 0:
        return 0.5 * (rekurencyP(i - 1, j) + rekurencyP(i, j - 1))



def dynamicP(k,l):
    position = {} # tablica 2D w postaci słownika
    if k == 0 and l ==0:
        return 0.5
    for i in range(k+1):
        position[(i, 0)] = 0
    for i in range(l+1):
        position[(0, i)] = 1
    for i in range(1,k+1):
        for j in range(1,l+1):
            position[(i, j)] = 0.5 * (position[(i - 1, j)] + position[(i, j - 1)])
    return position[(k,l)]



class MyTestCase(unittest.TestCase):

    def test_recurencyP(self):
        self.assertEqual(rekurencyP(0, 0), 0.5)
        self.assertEqual(rekurencyP(3, 0), 0)
        self.assertEqual(rekurencyP(2, 3), 0.6875)

    def test_dynamicP(self):
        #self.assertEqual(dynamicP(0, 0), 0.5)
        self.assertEqual(dynamicP(3, 0), 0)
        self.assertEqual(dynamicP(2, 3), 0.6875)


if __name__ == '__main__':
    unittest.main()