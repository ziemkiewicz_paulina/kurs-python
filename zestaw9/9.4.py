import copy


class SortedList:
    '''
    Przechowywanie listy od wartosci najmniejszej do największej
    '''

    def print(self, alreadyExistinglist=""):
        listString = alreadyExistinglist
        listString += str(self.value) + ", "
        if self.next == None:
            print(listString)
        else:
            return self.next.print(listString)

    def __str__(self):
        return str("Node: " + str(self.value))

    def __init__(self, value=None):
        self.value = value
        self.next = None

    def is_empty(self):
        if self.value == None:
            return True
        else:
            return False

    def insert(self, node):
        if node.next != None:
            raise ValueError
        value = node.value
        if self.value != None:
            if self.value <= value:
                if self.next != None:
                    self.next.insert(SortedList(value))
                if self.next == None:
                    self.next = SortedList(value)
            else:
                temp =self.next
                tempValue = self.value
                self.value = value
                self.next = SortedList(tempValue)
                self.next.next = temp
        else:
            self.value = value

    def remove(self):
        if self.next == None:
            temp = copy.deepcopy(self)
            self.clear()
            return temp

        if self.next.next == None:
            temp = self.next
            self.next = None
            return temp
        else:
            return self.next.remove()

    def merge(self, other):
        listOfOtherNodes = []
        while True:
            if other.value == None:
                break
            listOfOtherNodes.append(other.remove())
        for element in listOfOtherNodes:
            self.insert(element)

    def clear(self):
        self.value = None
        self.next = None

    def add(self, intValue):
        self.insert(SortedList(intValue))


a = SortedList(1)
a.add(3)
a.add(5)
a.add(7)

b = SortedList(2)
b.add(4)
b.add(6)

a.merge(b)
a.print()
