class BinaryTree:
    def __init__(self, value=None):
        self.left = None
        self.right = None
        self.value = value

    def addRight(self, value):
        if self.right == None:
            self.right = BinaryTree(value)
        else:
            self.right.addRight(value)

    def addLeft(self, value):
        if self.left == None:
            self.left = BinaryTree(value)
        else:
            self.left.addLeft(value)


def count_leafs(top):
    number_of_leafs = 0
    if top.left == None and top.right == None:
        return 1
    if top.left != None:
        number_of_leafs += count_leafs(top.left)
    if top.right != None:
        number_of_leafs += count_leafs(top.right)
    return number_of_leafs


def calc_total(top):
    sum_of_values = 0
    if top.left == None and top.right == None:
        return top.value
    if top.left != None:
        sum_of_values += calc_total(top.left)
    if top.right != None:
        sum_of_values += calc_total(top.right)
    sum_of_values += top.value
    return sum_of_values


a = BinaryTree(20)
a.addRight(4)
a.right.addRight(5)
a.right.addLeft(6)
a.addLeft(10)
a.left.addRight(5)

print("liczba lisci: ", count_leafs(a))

print("suma wartosci w drzewie: ", calc_total(a))
